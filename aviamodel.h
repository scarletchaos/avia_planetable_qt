#ifndef AVIAMODEL_H
#define AVIAMODEL_H

#include <QAbstractListModel>
#include "FlightPlan.hpp"
#include "FlightPlanRepo.hpp"

class AviaModel : public QAbstractListModel
{
    Q_OBJECT

public:
    explicit AviaModel(QObject *parent = nullptr);

    enum Roles {
        DepartureAirportRole = Qt::UserRole,
        ArrivalAirportRole = Qt::UserRole + 1,
        AircraftNameRole = Qt::UserRole + 2,
        DepartureTimeRole = Qt::UserRole + 3
    };

    // Basic functionality:
    int rowCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    // Editable:
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role = Qt::EditRole) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;

    virtual QHash<int, QByteArray> roleNames() const override;
    void updateOffset(int offset) {offset_ = offset;}
protected:
    bool canFetchMore(const QModelIndex &parent) const override;
    void fetchMore(const QModelIndex &parent) override;

private:
    mutable FlightPlanRepo repo_;
    mutable std::shared_ptr<std::vector<FlightPlan>> plans_ptr_;
    mutable int offset_;
};

#endif // AVIAMODEL_H
