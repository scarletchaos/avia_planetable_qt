#pragma once

#include <memory>
#include <mutex>
#include <QObject>
#include <QFuture>

class FlightPlan;

class FlightPlanRepo {
public:
    FlightPlanRepo();
    Q_DISABLE_COPY(FlightPlanRepo)
    QFuture<std::shared_ptr<std::vector<FlightPlan>>> load100(size_t offset) noexcept;
    size_t getListSize() { return demoList_.size();}
private:
    std::mutex lock_;
    std::vector<FlightPlan> demoList_;
};
