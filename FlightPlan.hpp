#pragma once

#include <utility>
#include <vector>
#include <optional>
#include <QObject>
#include <QDateTime>

class PosId {
public:
    PosId() = default;
    explicit PosId(QString id)
        : id_(std::move(id))
    {
    }
    Q_DISABLE_COPY(PosId)
    PosId(PosId &&) = default;
    PosId &operator=(PosId &&) = default;
    QString id() const noexcept { return id_; }

private:
    QString id_;
};

class FlightPlan {
public:
    FlightPlan() = default;
    Q_DISABLE_COPY(FlightPlan)
    FlightPlan(FlightPlan &&) = default;
    FlightPlan &operator=(FlightPlan &&) = default;

    std::optional<QDateTime> departureTime() const noexcept { return departureTime_; }
    void setDepartureTime(QDateTime dt) { departureTime_ = dt; }
    void setAircraftName(QString name) {aircraftName_ = name; }
    void setDepartureArrival(QString dep, QString arr) {departureAirport_ = dep; arrivalAirport_ = arr; }
    QString getAircraftName() const noexcept { return aircraftName_; }
    QString getDepartureAirport() const noexcept { return departureAirport_; }
    QString getArrivalAirport() const noexcept { return arrivalAirport_; }


    size_t len() const noexcept { return points_.size(); }
    void push(PosId point) noexcept { points_.push_back(std::move(point)); }
    const PosId &getChecked(size_t i) { return points_.at(i); }
    FlightPlan clone() const noexcept
    {
        FlightPlan ret;
        ret.departureTime_ = departureTime_;
        ret.points_.reserve(points_.size());
        ret.aircraftName_ = aircraftName_;
        ret.departureAirport_ = departureAirport_;
        ret.arrivalAirport_ = arrivalAirport_;
        for (const auto &p : points_)
            ret.points_.emplace_back(p.id());
        return ret;
    }

private:
    std::optional<QDateTime> departureTime_;
    std::vector<PosId> points_;
    QString aircraftName_;
    QString departureAirport_;
    QString arrivalAirport_;
};
