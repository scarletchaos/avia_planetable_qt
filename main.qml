import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick 2


ApplicationWindow {
    id: win
    width: 1920
    height: 1080
    visible: true
    title: qsTr("Полёты")

    AviaList {
        id: avlist
        x: 0
        y: 0
        width: parent.width /2.5
        height: parent.height
        anchors.left: parent.left
        data: [
            Connections {
            }
        ]
    }
}
