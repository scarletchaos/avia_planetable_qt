#include "FlightPlanRepo.hpp"

#include <algorithm>
#include <QDateTime>
#include <QtConcurrent>

#include "FlightPlan.hpp"

namespace {
constexpr size_t N_ITEMS = 350;
constexpr unsigned long DELAY_SECS = 3;
} // namespace

FlightPlanRepo::FlightPlanRepo()
{
    const auto now = QDateTime::currentDateTimeUtc();

    const auto start = now.addDays(-N_ITEMS);

    for (size_t i = 0; i < N_ITEMS; ++i) {
        FlightPlan fp;
        fp.push(PosId(QString::number(i)));
        fp.push(PosId(QString::number(i + 1)));
        fp.setDepartureTime(start.addDays(i));
        fp.setAircraftName(QString("Boeing-737"));
        fp.setDepartureArrival(QString("SVO"), QString("KRR"));
        demoList_.push_back(std::move(fp));
    }
}

QFuture<std::shared_ptr<std::vector<FlightPlan>>>
FlightPlanRepo::load100(size_t offset) noexcept
{
    return QtConcurrent::run([offset, this] {
        QThread::sleep(DELAY_SECS);
        auto ret = std::make_shared<std::vector<FlightPlan>>();
        ret->reserve(100);
        {
            std::lock_guard<std::mutex> guard(lock_);
            for (size_t i = offset, n = std::min(demoList_.size(), offset + 100); i < n;
                 ++i) {
                ret->push_back(demoList_[i].clone());
            }
        }
        return ret;
    });
}
