#include "aviamodel.h"
#include <iostream>

AviaModel::AviaModel(QObject *parent)
    : QAbstractListModel(parent)
{
    offset_ = 0;
    plans_ptr_ = (repo_.load100(offset_).result());
    offset_ += 100;
}


int AviaModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return plans_ptr_->size();
}

QVariant AviaModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= offset_ || index.row() < 0)
        return QVariant();
    std::vector<FlightPlan>& plans = *plans_ptr_;
    switch (role) {
    case DepartureAirportRole:
        return QVariant(plans[index.row()].getDepartureAirport());
    case ArrivalAirportRole:
        return QVariant(plans[index.row()].getArrivalAirport());
    case AircraftNameRole:
        return QVariant(plans[index.row()].getAircraftName());
    case DepartureTimeRole:
        return QVariant((plans[index.row()].departureTime()).value());
    }
    return QVariant();
}

bool AviaModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (data(index, role) != value) {
        emit dataChanged(index, index, QVector<int>() << role);
        return true;
    }
    return false;
}

Qt::ItemFlags AviaModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

QHash<int, QByteArray> AviaModel::roleNames() const
{
    static QHash<int, QByteArray> mapping {
        {DepartureAirportRole, "departure"},
        {ArrivalAirportRole, "arrival"},
        {AircraftNameRole, "name"},
        {DepartureTimeRole, "deptime"}
    };
    return mapping;
}


bool AviaModel::canFetchMore(const QModelIndex &parent) const
{
    if (parent.isValid()){
        return false;
    }
    return (offset_ < int(repo_.getListSize()));
}

void AviaModel::fetchMore(const QModelIndex &parent)
{
    std::vector<FlightPlan>& plans = *plans_ptr_;
    std::shared_ptr<std::vector<FlightPlan>> tmp_ptr = (repo_.load100(offset_).result());
    std::vector<FlightPlan>& tmp = *tmp_ptr;
    auto res_ptr = std::make_shared<std::vector<FlightPlan>>();
    this->beginInsertRows(parent, offset_, offset_ + tmp.size()-1);
    offset_ += tmp.size();
    res_ptr->reserve(offset_);
    for (int i = 0; i < int(plans.size()); i++){
        res_ptr->push_back(std::move(plans[i]));
    }
    for (int i = 0; i < int(tmp.size()); i++){
        res_ptr->push_back(std::move(tmp[i]));
    }
    plans_ptr_ = res_ptr;
    this->endInsertRows();
}
