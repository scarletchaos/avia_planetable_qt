import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

import Avia 1.0

Frame {
    Component {
        id: nameDelegate
        Text {
            clip: true
            width: view.width
            wrapMode: TextEdit.WordWrap
            text: "Из " + "<font color=\"#0000FF\">" + model.departure + "</font>" +
                  " в " + "<font color=\"#0000FF\">" + model.arrival + "</font>" + "<br>" +
                  "Отправление " + "<font color=\"#0000FF\">" + model.deptime + "</font>" + "<br>" +
                  model.name
            font.pixelSize: 20
            font.family: "Helvetica"
            MouseArea {
                anchors.fill: parent
                onClicked: view.currentIndex = index
            }
        }
    }

    ListView {
        id: view
        x: 0
        y: 0
        anchors.fill: parent
        //        section.property: model.deptime
        //        section.criteria: ViewSection.FirstCharacter
        //        section.delegate: Rectangle{
        //            color: "#b0dfb0"
        //            width: parent.width
        //            implicitHeight: text.implicitHeight + 4
        //            Text {
        //                id: text
        //                anchors.centerIn: parent
        //                font.pixelSize: 32
        //                font.bold: true
        //                text: section
        //            }
        //        }

        model: AviaModel {}
        delegate: nameDelegate
        focus: true
        clip: true
        highlightMoveDuration: 0
        highlight: Rectangle{
            anchors {left: parent.left; right: parent.right }
            color: "lightgrey"
        }
    }
}
